import Person from "./Person";
import NewPersonForm from "./NewPersonForm";
const Persons = () => {
  const persons = [
    {
      name: "petar",
      surname: "petrovic",
      age: 27
    },
    {
      name: "marko",
      surname: "markovic",
      age: 30
    },
    {
      name: "jovan",
      surname: "jovanovic",
      age: 56
    },
  ]

  const addPersonHandler = (person) => {
    console.log("Add person handler");
    console.log(person);
  }

  return (
    <>
      <NewPersonForm onAddPerson={addPersonHandler}/>
      {persons.map((person) => <Person key={Math.random().toString()} name={person.name} surname={person.surname} age={person.age} />
      )}
    </>
  );
}

export default Persons;